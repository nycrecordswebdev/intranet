from flask_wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class DeleteForm(Form):
    submit = SubmitField('Yes')


class CommentForm(Form):
    body = StringField('', validators=[DataRequired()])
    submit = SubmitField('Submit')
