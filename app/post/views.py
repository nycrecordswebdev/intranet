"""
.. module:: post.views.

   :synopsis: Handles all post URL endpoints for the
   intranet application
"""
import datetime
import os
from datetime import timedelta, datetime
from os import listdir
from os import environ
from os.path import isfile, join
from app import db
from app.post import post
from app.models import Post, Tag, Permission, PostTag, Comment
from app.db_helpers import create_object, delete_object, allowed_post_file
from app.decorators import permission_required
from app.post.forms import DeleteForm, CommentForm
from flask import render_template, redirect, url_for, abort, flash, request
from flask_login import login_required, current_user
from werkzeug import secure_filename


@post.route('/new_post', methods=['GET', 'POST'])
@login_required
@permission_required(Permission.WRITE_ARTICLES)
def new_post(data=None):
    """
    Return the page for creating a new post containing tags, title, and text.

    Keyword arguments:
    data -- initialization of data (default None)
    """
    # tag_list = Tag.query.with_entities(Tag.name).all()
    # tag_list = [r[0].encode('utf-8') for r in tag_list]
    if data or request.method == 'POST':
        data = request.form.copy()
        post = Post.query.filter_by(text=data['editor1']).first()
        if data['editor1'] == "":
            return render_template('error.html', message="Please fill out the text.")
        if post is None:
            # if data['input_tag'] == '':
            #     return render_template('error.html', message="Please include tags!")
            title = data['input_title']
            text = data['editor1']
            file = request.files['post_file']
            if len(text) > 8000:
                return render_template('error.html',
                                       message='Text is too long. Please lower number of characters or remove some text formatting.')
            time = datetime.now()
            post = Post(title=title, text=text, time=time, author=current_user._get_current_object())
            create_object(post)
            if len(file.filename) > 0:
                post_file = "postid" + str(post.id) + "_" + secure_filename(file.filename.replace(" ", "_"))
                save_address = os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), post_file)
                # print(save_address)
                if allowed_post_file(file.filename):
                    # if post.file is not None:
                    #     old_file = os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), post.file)
                    #     if os.path.exists(old_file):
                    #         os.remove(old_file)
                    file.save(save_address)
                    post.file = "postid" + str(post.id) + "_" + str(file.filename.replace(" ", "_"))
                    db.session.commit()
                    # post_folder_files = [f for f in listdir(os.environ.get('UPLOAD_POST_FILE_FOLDER')) if
                    #                      isfile(join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), f))]
                    # print(post_folder_files)
                else:
                    return render_template('error.html', message="Please upload a valid file type.")
        # tag_split = data['input_tag'].split(', ')
        # for each_tag in tag_split:
        #     if Tag.query.filter_by(name='').first() != None:
        #         delete_object(Tag.query.filter_by(name='').first())
        #     if each_tag[0] != '#':
        #         each_tag = '#' + each_tag
        #     if each_tag not in tag_list:
        #         newtag = Tag(name=each_tag)
        #         create_object(newtag)
        #         posttag = PostTag(post_id=post.id, tag_id=newtag.id)
        #         create_object(posttag)
        #     else:
        #         oldtag = Tag.query.filter_by(name=each_tag).first().id
        #         posttag = PostTag(post_id=post.id, tag_id=oldtag)
        #         create_object(posttag)
        return redirect(url_for('main.index'))
    # return render_template('post/new_post.html', tag_list=tag_list)
    return render_template('post/new_post.html')


@post.route('/<int:id>', methods=['GET', 'POST'])
def view_post(id):
    """
    Return the page of a specific post containing the post itself, comments, and comment box if logged in.

    Keyword arguments:
    id -- the post id
    """
    post = Post.query.get_or_404(id)
    page_posts = []
    if post.author is None:
        author = ''
    else:
        author = post.author.first_name + ' ' + post.author.last_name
    post_tags = PostTag.query.filter_by(post_id=post.id).all()
    tags = []
    for tag in post_tags:
        name = Tag.query.filter_by(id=tag.tag_id).first().name
        tag = {
            'id': tag.tag_id,
            'name': name
        }
        tags.append(tag)
    single_post = {
        'id': post.id,
        'title': post.title,
        'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
        'text': post.text,
        'comments': post.comments.count(),
        'tags': tags,
        'author': author,
        'file': post.file
    }
    page_posts.append(single_post)
    form = CommentForm()
    allComments = []
    for comment in post.comments:
        allComments.append(comment)
    allComments.reverse()
    allCommentsCount = len(allComments)
    if form.validate_on_submit():
        comment = Comment(body=form.body.data, post=post, author=current_user._get_current_object())
        create_object(comment)
        flash('Your comment has been posted.')
        return redirect(url_for('post.view_post', id=post.id, page=-1))
    page = request.args.get('page', 1, type=int)
    if page == -1:
        page = (post.comments.count() - 1) / \
               int(os.environ.get('COMMENTS_PER_PAGE')) + 1
    pagination = post.comments.order_by(Comment.timestamp.asc()).paginate(
        page, per_page=int(os.environ.get('COMMENTS_PER_PAGE')), error_out=False)
    comments = pagination.items
    return render_template('post/post.html', post=post, posts=[post], form=form, allComments=allComments,
                           allCommentsCount=allCommentsCount, comments=comments, pagination=pagination,
                           page_posts=page_posts)


@post.route('/tag/<string:tag>', methods=['GET', 'POST'])
def tag(tag):
    """
    Return the page containing all posts with the chosen tag.

    Keyword arguments:
    tag -- the selected tag
    """
    page_posts = []
    tagid = Tag.query.filter_by(name=tag).first().id
    posttags = PostTag.query.filter_by(tag_id=tagid).all()
    posts = []
    for posttag in posttags:
        post = Post.query.filter_by(id=posttag.post_id).first()
        posts.append(post)
    posts.reverse()
    for post in posts:
        if post.author is None:
            author = ''
        else:
            author = post.author.first_name + ' ' + post.author.last_name
        post_tags = PostTag.query.filter_by(post_id=post.id).all()
        tags = []
        for tag in post_tags:
            name = Tag.query.filter_by(id=tag.tag_id).first().name
            tag = {
                'id': tag.tag_id,
                'name': name
            }
            tags.append(tag)
        post = {
            'id': post.id,
            'title': post.title,
            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
            'text': post.text,
            'comments': post.comments.count(),
            'tags': tags,
            'author': author,
            'file': post.file
        }
        page_posts.append(post)
    return render_template('post/tagged_posts.html', page_posts=page_posts)


@post.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    """
    Return the page for editing a specific post containing possible edits for tags, title, and text.

    Keyword arguments:
    id -- the post id
    """
    # tag_list = Tag.query.with_entities(Tag.name).all()
    # tag_list = [r[0].encode('utf-8') for r in tag_list]
    # previous_tag_string = ""
    # previous_tag_list = []
    # postids = PostTag.query.filter_by(post_id=id).all()
    # for postid in postids:
    #     tagid = postid.tag_id
    #     tagname = Tag.query.filter_by(id=tagid).first().name
    #     previous_tag_list.append(tagname)
    #     previous_tag_string += tagname
    #     previous_tag_string += ", "
    # previous_tag_string = previous_tag_string[:-2]
    post = Post.query.get_or_404(id)
    if current_user != post.author and not current_user.can(Permission.ADMINISTER):
        abort(403)
    if request.method == 'POST':
        data = request.form.copy()
        post.title = data['input_title']
        post.text = data['editor1']
        create_object(post)
        file = request.files['post_file']
        if len(file.filename) > 0:
            post_file = "postid" + str(post.id) + "_" + secure_filename(file.filename.replace(" ", "_"))
            save_address = os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), post_file)
            # print("save_address: ", save_address)
            if allowed_post_file(file.filename):
                if post.file is not None:
                    old_file = os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), post.file)
                    if os.path.exists(old_file):
                        os.remove(old_file)
                file.save(save_address)
                post.file = "postid" + str(post.id) + "_" + str(file.filename.replace(" ", "_"))
                db.session.commit()
                # post_folder_files = [f for f in listdir(os.environ.get('UPLOAD_POST_FILE_FOLDER')) if
                #                      isfile(join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), f))]
                # print(post_folder_files)
            else:
                return render_template('error.html', message="Please upload a valid file type.")
        # tag_split = data['input_tag'].split(', ')
        # for each_tag in tag_split:
        #     if Tag.query.filter_by(name='').first() is not None:
        #         delete_object(Tag.query.filter_by(name='').first())
        #     if len(tag_split[0]) != 0 and each_tag[0] != '#':
        #         each_tag = '#' + each_tag
        #     if each_tag not in tag_list:
        #         newtag = Tag(name=each_tag)
        #         create_object(newtag)
        #         posttag = PostTag(post_id=post.id, tag_id=newtag.id)
        #         create_object(posttag)
        #     elif each_tag not in previous_tag_list:
        #         oldtag = Tag.query.filter_by(name=each_tag).first().id
        #         posttag = PostTag(post_id=post.id, tag_id=oldtag)
        #         create_object(posttag)
        # for each_tag in previous_tag_list:
        #     if each_tag not in tag_split:
        #         oldtag = Tag.query.filter_by(name=each_tag).first().id
        #         PostTag.query.filter_by(post_id=post.id, tag_id=oldtag).delete()
        #         db.session.commit()
        flash('The post has been updated.')
        return redirect(url_for('post.view_post', id=post.id))
    # return render_template('post/edit_post.html', post=post, tag_list=tag_list, previous_tag_string=previous_tag_string)
    return render_template('post/edit_post.html', post=post)


@post.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete(id):
    """
    Return the confirmation page for deleting a post.

    Keyword arguments:
    id -- the post id
    """
    post = Post.query.get_or_404(id)
    if current_user != post.author and not current_user.can(Permission.ADMINISTER) and not current_user.is_director():
        abort(403)
    form = DeleteForm()
    if form.validate_on_submit():
        posttags = PostTag.query.filter_by(post_id=id).all()
        for posttag in posttags:
            delete_object(posttag)
        comments = Comment.query.filter_by(post_id=id).all()
        for comment in comments:
            delete_object(comment)
        delete_object(post)
        file = post.file
        if file is not None:
            if os.path.isfile(os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), file)):
                os.remove(os.path.join(os.environ.get('UPLOAD_POST_FILE_FOLDER'), file))
        db.session.commit()
        flash('The post has been deleted.')
        return redirect(url_for('main.index'))
    return render_template('post/delete_post.html', form=form)
