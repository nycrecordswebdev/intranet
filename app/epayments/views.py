import os
from datetime import datetime
from flask import render_template, redirect, url_for, request, current_app

from app.epayments import epayments
from app.epayments.utils import (
    allowed_file,
    import_xml as import_file
)


@epayments.route('/', methods=['GET', 'POST'])
def index():
    """Default route for the application."""
    return render_template('epayments/index.html')


@epayments.route('/import', methods=['GET', 'POST'])
def import_xml():
    """Import Orders into the database."""
    if request.method == 'POST':
        file_ = request.files['file']
        if file_ and allowed_file(file_.filename):
            actual_filename = 'DOR-{date}.tar'.format(date=datetime.now().strftime('%Y-%m-%d'))
            filename = os.path.join(current_app.config['LOCAL_FILE_PATH'], actual_filename)
            file_.save(filename)
            import_file(filename)
            return redirect(url_for('epayments.index'))
    return render_template('epayments/import.html')
