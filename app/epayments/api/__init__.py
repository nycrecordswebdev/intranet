from flask import Blueprint

epayments_api_blueprint = Blueprint('epayments_api_blueprint', __name__)

from . import views
