"""
.. module:: utils.

   :synopsis: Utility functions used throughout the application.
"""

divisions = [
    ('', ''),
    ('Administration', 'Administration'),
    ('Executive', 'Executive'),
    ('Grants Administration', 'Grants Administration'),
    ('IT', 'IT'),
    ('Municipal Archives', 'Municipal Archives'),
    ('Municipal Library', 'Municipal Library'),
    ('Municipal Records Management', 'Municipal Records Management')
]

roles = [
    ('Employee', 'Employee'),
    ('Director', 'Director'),
    ('Administrator', 'Administrator')
]


class InvalidResetToken(Exception):
    pass