"""
.. module:: main.views.

   :synopsis: Handles all main URL endpoints for the
   intranet application
"""
import tweepy
import os
from datetime import timedelta, datetime
from flask import render_template, redirect, url_for, request, session
from app.main import main
from app.main.forms import NameForm
from app.db_helpers import create_object
from app.models import Post, Tag, PostTag


@main.route('/', methods=['GET', 'POST'])
def index():
    """
    Return the main homepage containing all submitted posts with their corresponding tags,
    search bar, and a new post button.

    Only directors or administrators will be able to see or use the new post feature.
    """
    page = request.args.get('page', 1, type=int)
    pagination = Post.query.order_by(Post.time.desc()).paginate(page, per_page=int(os.environ.get('POSTS_PER_PAGE')),
                                                                error_out=False)
    posts = pagination.items
    all_tags = Tag.query.all()
    page_posts = []
    # auth = tweepy.OAuthHandler(os.environ.get('consumer_key'), os.environ.get('consumer_secret'))
    # auth.set_access_token(os.environ.get('auth_token'), os.environ.get('auth_secret'))
    # api = tweepy.API(auth)
    # recent_tweet = api.user_timeline(screen_name='nycrecords', count=1, include_rts=True)
    # for tweet in recent_tweet:
    #     tweet_datetime = (tweet.created_at - timedelta(hours=4)).strftime('%B %d, %Y %l:%M%p')
    #     if Post.query.filter_by(text=tweet.text).first() is None:
    #         tweet_title = 'Twitter - @nycrecords: ' + str((tweet.created_at - timedelta(hours=4)).strftime('%B %d, %Y'))
    #         post = Post(title=tweet_title, text=tweet.text, time=(tweet.created_at - timedelta(hours=4)))
    #         create_object(post)
    #         add_twitter_tag = Tag(name='#twitter')
    #         create_object(add_twitter_tag)
    #         twitter_tag = PostTag(post_id=post.id, tag_id=Tag.query.filter_by(name='#twitter').first().id)
    #         create_object(twitter_tag)
    for post in posts:
        if post.author is None:
            author = ''
        else:
            author = post.author.first_name + ' ' + post.author.last_name
        post_tags = PostTag.query.filter_by(post_id=post.id).all()
        tags = []
        for tag in post_tags:
            name = Tag.query.filter_by(id=tag.tag_id).first().name
            tag = {
                'id': tag.tag_id,
                'name': name
            }
            tags.append(tag)
        post = {
            'id': post.id,
            'title': post.title,
            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
            'text': post.text,
            'comments': post.comments.count(),
            'tags': tags,
            'author': author,
            'file': post.file
        }
        page_posts.append(post)
    for page_post in page_posts:
        for tag in page_post['tags']:
            if tag['name'] == "#sticky":
                page_posts.insert(0, page_posts.pop(page_posts.index(page_post)))
    if request.method == 'POST':
        page_posts = []
        search_term = request.form.get('search_term')
        search_option = request.form.get('select_search_option')
        select_tags = request.form.getlist('select_tags')
        if search_term == '':
            if select_tags == '':
                return render_template('index.html', pagination=pagination, page_posts=page_posts,
                                       all_tags=all_tags)
        else:
            if search_option == 'all':
                for post in Post.query.all():
                    if search_term in post.title:
                        if post.author is None:
                            author = ''
                        else:
                            author = post.author.first_name + ' ' + post.author.last_name
                        post_tags = PostTag.query.filter_by(post_id=post.id).all()
                        tags = []
                        for tag in post_tags:
                            name = Tag.query.filter_by(id=tag.tag_id).first().name
                            tag = {
                                'id': tag.tag_id,
                                'name': name
                            }
                            tags.append(tag)
                        post = {
                            'id': post.id,
                            'title': post.title,
                            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
                            'text': post.text,
                            'comments': post.comments.count(),
                            'tags': tags,
                            'author': author,
                            'file': post.file
                        }
                        page_posts.append(post)
                    elif search_term in post.text:
                        if post.author is None:
                            author = ''
                        else:
                            author = post.author.first_name + ' ' + post.author.last_name
                        post_tags = PostTag.query.filter_by(post_id=post.id).all()
                        tags = []
                        for tag in post_tags:
                            name = Tag.query.filter_by(id=tag.tag_id).first().name
                            tag = {
                                'id': tag.tag_id,
                                'name': name
                            }
                            tags.append(tag)
                        post = {
                            'id': post.id,
                            'title': post.title,
                            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
                            'text': post.text,
                            'comments': post.comments.count(),
                            'tags': tags,
                            'author': author,
                            'file': post.file
                        }
                        page_posts.append(post)
            elif search_option == 'title':
                for post in Post.query.all():
                    if search_term in post.title:
                        if post.author is None:
                            author = ''
                        else:
                            author = post.author.first_name + ' ' + post.author.last_name
                        post_tags = PostTag.query.filter_by(post_id=post.id).all()
                        tags = []
                        for tag in post_tags:
                            name = Tag.query.filter_by(id=tag.tag_id).first().name
                            tag = {
                                'id': tag.tag_id,
                                'name': name
                            }
                            tags.append(tag)
                        post = {
                            'id': post.id,
                            'title': post.title,
                            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
                            'text': post.text,
                            'comments': post.comments.count(),
                            'tags': tags,
                            'author': author,
                            'file': post.file
                        }
                        page_posts.append(post)
            elif search_option == 'text':
                for post in Post.query.all():
                    if search_term in post.text:
                        if post.author is None:
                            author = ''
                        else:
                            author = post.author.first_name + ' ' + post.author.last_name
                        post_tags = PostTag.query.filter_by(post_id=post.id).all()
                        tags = []
                        for tag in post_tags:
                            name = Tag.query.filter_by(id=tag.tag_id).first().name
                            tag = {
                                'id': tag.tag_id,
                                'name': name
                            }
                            tags.append(tag)
                        post = {
                            'id': post.id,
                            'title': post.title,
                            'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
                            'text': post.text,
                            'comments': post.comments.count(),
                            'tags': tags,
                            'author': author,
                            'file': post.file
                        }
                        page_posts.append(post)
        select_tags = request.form.getlist('select_tags')
        for tag in select_tags:
            tagid = Tag.query.filter_by(name=tag).first().id
            posttags = PostTag.query.filter_by(tag_id=tagid).all()
            posts = []
            for posttag in posttags:
                post = Post.query.filter_by(id=posttag.post_id).first()
                posts.append(post)
            posts.reverse()
            for post in posts:
                if post.author is None:
                    author = ''
                else:
                    author = post.author.first_name + ' ' + post.author.last_name
                post_tags = PostTag.query.filter_by(post_id=post.id).all()
                tags = []
                for tag in post_tags:
                    name = Tag.query.filter_by(id=tag.tag_id).first().name
                    tag = {
                        'id': tag.tag_id,
                        'name': name
                    }
                    tags.append(tag)
                post = {
                    'id': post.id,
                    'title': post.title,
                    'time': post.time.strftime("%B %d, %Y %l:%M%p %Z"),
                    'text': post.text,
                    'comments': post.comments.count(),
                    'tags': tags,
                    'author': author,
                    'file': post.file
                }
                page_posts.append(post)
        page_posts_without_duplicates = []
        for page_post in page_posts:
            if page_post not in page_posts_without_duplicates:
                page_posts_without_duplicates.append(page_post)
        page_posts_without_duplicates.reverse()
        return render_template('post/tagged_posts.html', page_posts=page_posts_without_duplicates)
    return render_template('index.html', pagination=pagination, page_posts=page_posts,
                           all_tags=all_tags)


@main.route('/mis')
def mis():
    """
    Return the Management and Information Services information page.
    """
    return render_template('mis.html')


@main.route('/lmt')
def lmt():
    """
    Return the Labor Management Team information page.
    """
    return render_template('lmt.html')


@main.route('/archives')
def archives():
    """
    Return the Municipal Archives information page.
    """
    return render_template('archives.html')


@main.route('/library')
def library():
    """
    Return the Municipal Library information page.
    """
    return render_template('library.html')


@main.route('/agency_policies')
def agency_policies():
    """
    Return the agency policies page.
    """
    return render_template('agency_policies.html')


@main.route('/eeo')
def eeo():
    """
    Return the EEO information page.
    """
    return render_template('eeo.html')


@main.route('/error', methods=['GET', 'POST'])
def error():
    """
    Return the error page.
    """
    return render_template('error.html')


@main.route('/tweets')
def tweets():
    """
    Return all tweets posted on the intranet.
    """
    previous_tweets = []
    for post in Post.query.all():
        if 'Twitter - @nycrecords: ' in post.title:
            previous_tweets.append(post)
    for post in previous_tweets:
        post.time = post.time.strftime('%B %d, %Y %l:%M%p')
    previous_tweets.reverse()
    return render_template('tweets.html', previous_tweets=previous_tweets)


@main.route('/enfg/', methods=['GET', 'POST'])
def enfg():
    """
    Return the Easy Not Found Generator template.
    """
    form = NameForm()
    if form.validate_on_submit():
        session['type'] = form.type.data
        session['name'] = form.name.data
        session['bride_name'] = form.bride_name.data
        session['year'] = form.year.data
        session['borough'] = ', '.join(form.borough.data)
        session['signature'] = form.signature.data
        if not session['signature']:
            return redirect(url_for('main.result'))
        else:
            return redirect(url_for('main.result_nosig'))
    return render_template('enfg.html', form=form,
                           type=session.get('type'),
                           name=session.get('name'),
                           bride_name=session.get('bride_name'),
                           year=session.get('year'),
                           borough=session.get('borough'),
                           now=datetime.today())


@main.route('/enfg/result', methods=['GET', 'POST'])
def result():
    """
    Return the Easy Not Found Generator result.
    """
    session['date'] = datetime.today().strftime('%m/%d/%y')
    return render_template('result.html',
                           date=session.get('date'),
                           type=session.get('type'),
                           name=session.get('name'),
                           bride_name=session.get('bride_name'),
                           year=session.get('year'),
                           borough=session.get('borough'),
                           now=datetime.today())


@main.route('/enfg/result_nosig', methods=['GET', 'POST'])
def result_nosig():
    """
    Return the Easy Not Found Generator result without a signature.
    """
    session['date'] = datetime.today().strftime('%m/%d/%y')
    return render_template('result_nosig.html',
                           date=session.get('date'),
                           type=session.get('type'),
                           name=session.get('name'),
                           bride_name=session.get('bride_name'),
                           year=session.get('year'),
                           borough=session.get('borough'),
                           now=datetime.today())
