CLIENT_AGENCY_NAMES = {
    "10000048": "Photo Tax",
    "10000060": "Photo Gallery",
    "10000102": "Birth Search",
    "10000147": "Birth Cert",
    "10000104": "Marriage Search",
    "10000181": "Marriage Cert",
    "10000103": "Death Search",
    "10000182": "Death Cert",
    "10000058": "Property Card"
}

ORDER_TYPES = [
    'tax photo',
    'online gallery',
    'Property card'
    'Birth search',
    'Birth cert',
    'Marriage search',
    'Marriage cert',
    'Death search',
    'Death cert'
]

VITAL_RECORDS_ORDERS = {
    'Birth search',
    'Birth cert',
    'Marriage search',
    'Marriage cert',
    'Death search',
    'Death cert'
}

PHOTO_ORDERS = {
    'tax photo',
    'online gallery',
    'Property card'
}

MULTIPLE_ORDERS = [
    'All',
    'vitalrecords',
    'photos',
    'multipleitems',
    'vitalrecordsphotos'
]

ALL_VITAL_RECORDS = 'vitalrecords'

VITAL_RECORDS_LIST = [
    'Birth Search',
    'Birth Cert',
    'Marriage Search',
    'Marriage Cert',
    'Death Search',
    'Death Cert'
]

ALL_PHOTOS = 'photos'

PHOTO_ORDERS_LIST = [
    'tax photo',
    'online gallery',
    'Property card'
]

MULTIPLE_ITEMS_IN_CART = 'multipleitems'

VITAL_RECORDS_PHOTOS_ORDER = 'vitalrecordsphotos'

ALLOWED_EXTENSIONS = frozenset(['tar', 'xml'])
