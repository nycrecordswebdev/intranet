"""
.. module:: comment.views.

   :synopsis: Handles all comment URL endpoints for the
   intranet application
"""
from app.comment import comment
from app.comment.forms import DeleteForm, CommentForm
from app.db_helpers import create_object, delete_object
from app.models import Post, Permission, Comment
from app.decorators import permission_required
from flask import render_template, redirect, url_for, abort, flash
from flask_login import login_required, current_user


@comment.route('/moderate/<int:id>', methods=['GET', 'POST'])
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate(id):
    """
    Return the moderation page for comments from a specific post.

    Keyword arguments:
    id -- the post id
    """
    avatar = current_user.avatar
    post = Post.query.get_or_404(id)
    allComments = []
    comments = post.comments
    for comment in comments:
        allComments.append(comment)
    allComments.reverse()
    allCommentsCount = len(allComments)
    return render_template('comment/moderate.html', post=post.id, avatar=avatar, allComments=allComments,
                           allCommentsCount=allCommentsCount)


@comment.route('/moderate/enable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_enable(id):
    """
    Enable a comment and redirect to the moderation page.

    Keyword arguments:
    id -- the comment id
    """
    comment = Comment.query.get_or_404(id)
    comment.disabled = False
    create_object(comment)
    post = Comment.query.filter_by(id=id).first().post
    return redirect(url_for('comment.moderate', id=post.id))


@comment.route('/moderate/disable/<int:id>')
@login_required
@permission_required(Permission.MODERATE_COMMENTS)
def moderate_disable(id):
    """
    Disable a comment and redirect to the moderation page.

    Keyword arguments:
    id -- the comment id
    """
    comment = Comment.query.get_or_404(id)
    comment.disabled = True
    create_object(comment)
    post = Comment.query.filter_by(id=id).first().post
    return redirect(url_for('comment.moderate', id=post.id))


@comment.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_comment(id):
    """
    Return the page for editing a specific comment.

    Keyword arguments:
    id -- the comment id
    """
    comment = Comment.query.get_or_404(id)
    form = CommentForm()
    if current_user != comment.author and not current_user.can(Permission.COMMENT):
        abort(403)
    if form.validate_on_submit():
        if "[Edited]" not in form.body.data:
            comment.body = "[Edited] " + form.body.data
        else:
            comment.body = form.body.data
        create_object(comment)
        flash('The comment has been updated.')
        return redirect(url_for('post.view_post', id=comment.post_id))
    form.body.data = comment.body
    return render_template('comment/edit_comment.html', form=form, comment=comment)


@comment.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_comment(id):
    """
    Return the confirmation page for deleting a comment.

    Keyword arguments:
    id -- the comment id
    """
    comment = Comment.query.get_or_404(id)
    form = DeleteForm()
    if current_user != comment.author and not current_user.can(Permission.COMMENT):
        abort(403)
    if form.validate_on_submit():
        delete_object(comment)
        flash('The comment has been deleted.')
        return redirect(url_for('post.view_post', id=comment.post_id))
    return render_template('comment/delete_comment.html', form=form, comment=comment)
