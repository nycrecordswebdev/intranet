"""
.. module:: user.views.

   :synopsis: Handles all user URL endpoints for the
   intranet application
"""
import os
from app import db
from app.user import user
from app.models import User, Role
from app.db_helpers import delete_object, allowed_avatar_file
from flask import render_template, redirect, request, url_for, flash
from flask_login import login_required, current_user
from werkzeug import secure_filename


@user.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    """
    Return a page with ability to upgrade user information.

    Only administrators can upgrade user information.
    """
    users = User.query.all()
    divisions = ['Administration', 'Executive', 'Grants Administration', 'IT', 'Municipal Archives',
                 'Municipal Library', 'Municipal Records Management']
    roles = ['Employee', 'Director', 'Administrator']
    if request.method == 'POST':
        select_reset = request.form.getlist('select_reset')[0]
        select_delete = request.form.getlist('select_delete')[0]
        select_user_division = request.form.getlist('select_user_division')[0]
        select_user_role = request.form.getlist('select_user_role')[0]
        select_division = request.form.getlist('select_division')[0]
        select_role = request.form.getlist('select_role')[0]
        if (len(select_reset) == 0) and (len(select_delete) == 0) and (len(select_user_division) == 0) and (
                    len(select_division) == 0) and (len(select_user_role) == 0) and (len(select_role) == 0):
            return render_template('user/edit_user_info.html', users=users, divisions=divisions, roles=roles)
        else:
            if request.form['submit'] == 'Reset Password':
                if len(select_reset) > 0:
                    for selected_id in request.form.getlist('select_reset'):
                        account = User.query.get_or_404(selected_id)
                        account.password = 'Change4me'
                        flash('User password has been reset.')
            elif request.form['submit'] == 'Update Division':
                if (len(select_user_division) > 0) and (len(select_division) > 0):
                    selected_user = User.query.filter_by(email=select_user_division).first()
                    selected_user.division = select_division
                    flash('User division has been updated.')
            elif request.form['submit'] == 'Update Role':
                if (len(select_user_role) > 0) and (len(select_role) > 0):
                    selected_user = User.query.filter_by(email=select_user_role).first()
                    if select_role == 'Employee':
                        selected_user.role = Role.query.filter_by(name='Employee').first()
                    elif select_role == 'Director':
                        selected_user.role = Role.query.filter_by(name='Director').first()
                    elif select_role == 'Administrator':
                        selected_user.role = Role.query.filter_by(name='Administrator').first()
                    flash('User role has been updated.')
            elif request.form['submit'] == 'Delete Account':
                if len(select_delete) > 0:
                    for selected_id in request.form.getlist('select_delete'):
                        account = User.query.get_or_404(selected_id)
                        delete_object(account)
                        flash('Account has been deleted.')
            db.session.commit()
            users = User.query.all()
            return render_template('user/edit_user_info.html', users=users, divisions=divisions, roles=roles)
    return render_template('user/edit_user_info.html', users=users, divisions=divisions, roles=roles)


@user.route('/profile/<string:email>', methods=['GET', 'POST'])
@login_required
def profile(email):
    """
    Return the profile page of a user including the user's avatar, role,
    email, number of comments, and number of posts.

    Users can edit their email, and avatar by uploading a image file.
    """
    if User.query.filter_by(email=email).first() is None:
        return render_template('error.html', message='User not found.')
    user_id = User.query.filter_by(email=email).first().id
    role = User.query.filter_by(email=email).first().role.name
    email = User.query.filter_by(email=email).first().email
    division = User.query.filter_by(email=email).first().division
    posts = User.query.filter_by(email=email).first().posts.count()
    comments = User.query.filter_by(email=email).first().comments.count()
    avatar = User.query.filter_by(email=email).first().avatar
    first_name = User.query.filter_by(email=email).first().first_name
    last_name = User.query.filter_by(email=email).first().last_name
    if request.method == 'POST':
        file = request.files['profile_picture']
        if bool(file) is False:
            return render_template('user/profile.html', user_id=user_id, role=role, email=email, division=division,
                                   posts=posts, comments=comments, avatar=avatar, first_name=first_name,
                                   last_name=last_name)
        else:
            if bool(file) is True:
                avatar_file = "userid" + str(current_user.id) + secure_filename(file.filename.replace(" ", "_"))
                save_address = os.path.join(os.environ.get('UPLOAD_AVATAR_FOLDER'), avatar_file)
                if allowed_avatar_file(file.filename):
                    if current_user.avatar is not None and current_user.avatar != "avatars/default.png":
                        old_avatar = os.path.join(os.environ.get('UPLOAD_AVATAR_FOLDER'), current_user.avatar[8:])
                        if os.path.exists(old_avatar):
                            os.remove(old_avatar)
                        else:
                            pass
                    file.save(save_address)
                    current_user.avatar = 'avatars/' + "userid" + str(current_user.id) + str(
                        file.filename.replace(" ", "_"))
                    db.session.commit()
                    flash('Avatar has been updated.')
                else:
                    flash('The uploaded file cannot be used.')
            return redirect(url_for('user.profile', user_id=user_id, role=role, email=email, division=division,
                                    posts=posts, comments=comments, avatar=avatar, first_name=first_name,
                                    last_name=last_name))
    return render_template('user/profile.html', user_id=user_id, role=role, email=email, division=division, posts=posts,
                           comments=comments, avatar=avatar, first_name=first_name, last_name=last_name)
