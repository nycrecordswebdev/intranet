import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

dotenv_path = os.path.join(basedir, '.env')
load_dotenv(dotenv_path)

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = os.environ.get('SQLALCHEMY_COMMIT_ON_TEARDOWN')
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS')
    MAIL_PORT = os.environ.get('MAIL_PORT')
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_SUBJECT_PREFIX = os.environ.get('MAIL_SUBJECT_PREFIX')
    MAIL_SENDER = os.environ.get('MAIL_SENDER')
    ADMIN = os.environ.get('ADMIN')
    USE_LDAP = os.environ.get('USE_LDAP')
    LDAP_SERVER = os.environ.get('LDAP_SERVER')
    LDAP_PORT = os.environ.get('LDAP_PORT')
    LDAP_USE_TLS = os.environ.get('LDAP_USE_TLS')
    LDAP_CERT_PATH = os.environ.get('LDAP_CERT_PATH')
    LDAP_SA_BIND_DN = os.environ.get('LDAP_SA_BIND_DN')
    LDAP_SA_PASSWORD = os.environ.get('LDAP_SA_PASSWORD')
    LDAP_BASE_DN = os.environ.get('LDAP_BASE_DN')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    REMOTE_FILE_PATH = os.environ.get('REMOTE_FILE_PATH')
    LOCAL_FILE_PATH = (os.environ.get('LOCAL_FILE_PATH') or
                       os.path.join(os.path.abspath(os.path.dirname(__file__)), 'data/'))

    USE_SFTP = os.environ.get('USE_SFTP') == 'True'
    SFTP_HOSTNAME = os.environ.get('SFTP_HOSTNAME')
    SFTP_PORT = os.environ.get('SFTP_PORT')
    SFTP_USERNAME = os.environ.get('SFTP_USERNAME')
    SFTP_RSA_KEY_FILE = os.environ.get('SFTP_RSA_KEY_FILE')
    SFTP_UPLOAD_DIRECTORY = os.environ.get('SFTP_UPLOAD_DIRECTORY')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = (os.environ.get('DATABASE_URL') or
                               'postgresql://localhost:5432/intranet')


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost:5432/intranet'


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost:5432/intranet'


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
